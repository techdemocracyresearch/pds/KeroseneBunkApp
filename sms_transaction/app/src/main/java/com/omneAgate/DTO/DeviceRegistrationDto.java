package com.omneAgate.DTO;

import lombok.Data;

/**
 * Created by user1 on 5/3/15.
 */
@Data
public class DeviceRegistrationDto {

    LoginDto loginDto;

    DeviceDetailsDto deviceDetailsDto;


}
