package com.omneAgate.printer;

public interface Printer {

    public void print();

    public void discover();

}
