package com.omneAgate.DTO;

import lombok.Data;

/**
 * Created by user1 on 29/9/15.
 */

@Data
public class DistrictId {

    long id;
}
