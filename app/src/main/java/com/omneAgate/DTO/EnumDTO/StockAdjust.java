package com.omneAgate.DTO.EnumDTO;

/**
 * Created by user1 on 29/7/15.
 */
public enum StockAdjust {

    INCREMENT, DECREMENT
}
